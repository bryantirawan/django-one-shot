from django import template
from todos.models import TodoItem, TodoList

register = template.Library()

def getindex(lst, idx):
    return lst[idx]

register.filter(getindex)