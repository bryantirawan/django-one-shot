from django.urls import path
from todos.views import TodoItemUpdateView, TodoListDeleteView, TodoListDetailView, TodoListListView, TodoListCreateView, TodoListUpdateView, TodoItemCreateView

urlpatterns = [
    path("", TodoListListView.as_view(), name="todos_list"), 
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_detail"),
    path("create/", TodoListCreateView.as_view(), name="todolist_create"),
    path("<int:pk>/edit/", TodoListUpdateView.as_view(), name="todolist_update"),
    path("<int:pk>/delete/", TodoListDeleteView.as_view(), name="todolist_delete"),
    path("items/create/", TodoItemCreateView.as_view(), name="todoitem_create"),
    path("items/<int:pk>/edit/", TodoItemUpdateView.as_view(), name="todoitem_edit"),
]
