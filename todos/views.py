from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from todos.models import TodoList, TodoItem
from django.urls import reverse_lazy
from todos.templatetags.getindex import getindex
from django.db.models import Exists, OuterRef

# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    context_object_name = "todolist"
    template_name = "todos/list.html"

    # def get_context_data(self, **kwargs): 
    #     context = super().get_context_data(**kwargs) 
    #     allCompletedList = [] 
    #     for list in TodoList.objects.all(): 
            
    #         allCompleted = True
    #         for item in list.items.all(): 
    #             if item.is_completed == False: 
    #                 allCompleted = False 
    #         allCompletedList.append(allCompleted)
            
    #         context['allCompleted'] = allCompletedList
    #     return context 
    
    def get_queryset(self):
        queryset = super().get_queryset()
        subquery = TodoItem.objects.filter(
            list=OuterRef('pk'),
            is_completed=False
        )
        queryset = queryset.annotate(all_completed=~Exists(subquery))
        return queryset


class TodoListDetailView(DetailView): 
    model = TodoList 
    context_object_name = "todolist"
    template_name = "todos/detail.html"

    

class TodoListCreateView(CreateView): 
    model = TodoList
    template_name = "todos/create.html"
    fields = ["name"]
    # success_url = reverse_lazy("todos_list")

    def get_success_url(self):
        return reverse_lazy("todo_detail", args=[self.object.id])

class TodoListUpdateView(UpdateView): 
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("todos_list")

class TodoListDeleteView(DeleteView):
    model = TodoList 
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todos_list")

class TodoItemCreateView(CreateView): 
    model = TodoItem 
    template_name = "todos/items/create.html" 
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list")


class TodoItemUpdateView(UpdateView): 
    model = TodoItem
    template_name = "todos/items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list")